#include <algorithm>
#include <conio.h>
#include <iostream>
#include <fstream>
#include <math.h>
#include <vector>
#include <string>
#include <sstream>
#include <windows.h>

typedef void (*FunctionLst) (void);

int DrawHomePage();

void AttachFuncs();
int ChooseVariation(std::string* collection, int len); //
void EditNote();
void Empty() {};
void ExecuteComm(int key);
void ExecuteArrowOper(int st, int maxIdx, bool rightAndLeftAvailable);
int GetCurrId();
std::vector<std::string> GetData();
std::string* GetInput(); //
int GetNextUserId(); //
int GetNumberOfNotes();
std::vector<int> GetMarkedLaneIdxs(std::string);
bool HasConfirmed(); //
std::string HashPassword(std::string pass); //
bool IsEmptyFile(std::ifstream& file);
bool IsMarked(int opt);
bool LoginUser(std::string* UsernameAndPassword);
void Mark();
void NewNote();
void OpenHelp();
void OpenAuthenticationPage(); //
void RegisterUser(std::string* UsernameAndPass);
void RemoveNote();
void ReqIsDone();
void ReqRemoveNote();
void SaveLocally(std::string user, int id); //
void ShowCurrentNotes(std::vector<std::string>);
int TrackKeyboardPress();
bool RequireAuthentication(); //
void Unmark();
void ViewNote();
void Quit();

#define TITLE_PART "\n         TO DO List       \n  ------------------------\n  |No|       Notes       |\n  ------------------------\n"
#define LEFT_ARROW_IDX -2
#define RIGHT_ARROW_IDX -3

#define AUTHENTICATE_PAGE_IDX 0
#define TODOLIST_IDX 1

//ASCII codes
#define BACKSPACE 8
#define TAB 9
#define ENTER 13
#define ESCAPE 27
#define SPACE 32
#define UP_ARROW 72
#define LEFT_ARROW 75
#define RIGHT_ARROW 77
#define DOWN_ARROW 80
#define QUIT_KEY 113

int idx = 1;
int selectOption = idx;
FunctionLst Functions[26];
int isDone = 0, toRemove = 0;

int currTab = AUTHENTICATE_PAGE_IDX;

int main()
{
  system("MODE 28, 20");

  AttachFuncs();
  std::vector<std::string> data;
  while (1)
  {
    if (currTab == AUTHENTICATE_PAGE_IDX)
    {
      currTab = RequireAuthentication();
    } else if (currTab == TODOLIST_IDX)
    {
      data = GetData();
      ShowCurrentNotes(data);
      int key = TrackKeyboardPress();
      ExecuteComm(key);
    }
  //   std::cout << currTab << '\n';
  //   Sleep(1000);
  }

  Quit();
  return 0;
}

void AttachFuncs()
{
  for (int i = 0; i < 26; i++)
    Functions[i] = Empty;
  Functions['n' - 97] = NewNote;
  Functions['e' - 97] = EditNote;
  Functions['r' - 97] = ReqRemoveNote;
  Functions['h' - 97] = OpenHelp;
  Functions['v' - 97] = ViewNote;
  Functions['m' - 97] = Mark;
  Functions['d' - 97] = ReqIsDone;
  Functions['u' - 97] = Unmark;
  Functions['q' - 97] = Quit;
}

int ChooseVariation(std::string* collection, int len)
{
    int option = 0, keyCode = -1;

    HANDLE hConsole;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    system("cls");
    do {
        for (int i = 0; i < len; i++)
        {
            if (i == option)
                SetConsoleTextAttribute(hConsole, 9);
            else
                SetConsoleTextAttribute(hConsole, 15);

            std::cout << collection[i] << std::endl;
        }
        keyCode = getch();
        system("cls");
        if (keyCode == DOWN_ARROW)
            ++option;
        else if (keyCode == UP_ARROW)
            --option;

        if (option > len - 1)
            option = 0;
        else if (option < 0)
            option = len - 1;

        SetConsoleTextAttribute(hConsole, 15);
    } while (keyCode != ENTER);

    return option;
}

void EditNote()
{

}

void ExecuteComm(int key)
{
  if (key == 224)
    ExecuteArrowOper(idx, std::min(idx + 9, GetNumberOfNotes()), true);
  else if (key == QUIT_KEY)
    currTab = AUTHENTICATE_PAGE_IDX;
  else if (isalpha(char(key)))
    Functions[tolower(key) - 97]();
  else
    std::cout << char(7);
}

void ExecuteArrowOper(int st, int maxIdx, bool rightAndLeftAvailable)
{
  int key = getch();
  if (key == DOWN_ARROW)
  {
    if (selectOption == LEFT_ARROW_IDX || selectOption == RIGHT_ARROW_IDX)
      selectOption = maxIdx;
    else if (not (selectOption >= maxIdx))
      ++selectOption;
  } else if (key == LEFT_ARROW && rightAndLeftAvailable)
  {
    Sleep(100);
    selectOption = LEFT_ARROW_IDX;
    if (idx - 10 >= 1) idx -= 10;
  } else if (key == RIGHT_ARROW && rightAndLeftAvailable)
  {
      Sleep(100);
      selectOption = RIGHT_ARROW_IDX;
      if (idx + 10 <= 99) idx += 10;
  } else if (key == UP_ARROW)
  {
    if (selectOption == LEFT_ARROW_IDX || selectOption == RIGHT_ARROW_IDX)
      selectOption = idx;
    else if (not (selectOption <= st))
      --selectOption;
  }
}

int GetCurrId()
{
  std::ifstream file;
  file.open("./process/curr.txt");
  int id;
  std::string un;
  std::getline(file, un);
  file >> id;
  return id;
}

std::vector<std::string> GetData()
{
  std::ifstream file;
  std::string path = "./data/" + std::to_string(GetCurrId()) + ".txt";
  file.open(path, std::ios::out);
  int firstOptionIdx = idx;
  int lastOptionIdx = firstOptionIdx + 10;
  int i = 1;
  std::string lane;
  std::vector<std::string> data;
  while (!file.eof() && i < lastOptionIdx)
  {
      getline(file, lane);
      if (i >= firstOptionIdx && lane.size() != 0)
          data.push_back(lane);
      i++;
  }

  file.close();
  if (data.size() == 0 && idx > 10)
  {
    idx -= 10;
    return GetData();
  }

  return data;
}

std::string* GetInput()
{
    std::string Username, Password;
    std::cout << "Username: ";
    std::getline(std::cin, Username);
    std::cout << "Password: ";
    char symbol = 0;
    do
    {
        symbol = getch();
        Password += symbol;
        std::cout << ( (symbol != ENTER) ? "*" : "");
    } while (symbol != ENTER);

    std::string* UsernameAndPassword = new std::string[2];
    UsernameAndPassword[0] = Username;
    UsernameAndPassword[1] = Password;
    return UsernameAndPassword;
}

int GetNextUserId()
{
    int id = 0;
    std::string path;

    while(1)
    {
        path = "./acc/" + std::to_string(id) + ".txt";
        std::ifstream file(path);
        if (!file.good())
            break;
        id++;
    }

    return id;
}

int GetNumberOfNotes()
{
  int c = 0;
  std::string path = "./data/" + std::to_string(GetCurrId()) + ".txt";
  std::ifstream file;
  std::string lane;
  file.open(path);
  while (!file.eof())
  {
    getline(file, lane);
    if (lane == "") --c;
    ++c;
  }
  file.close();
  return c;
}

std::vector<int> GetMarkedLaneIdxs(std::string lane)
{
  std::vector<int> marked;
  std::string buffer;
  std::stringstream stream_ (lane);
  while (stream_ >> buffer)
    marked.push_back(std::stoi(buffer));

  sort(marked.begin(), marked.end());

  return marked;
}

bool HasConfirmed()
{
    std::string confirm;
    std::cout << "\nConfirm password: ";
    std::cin >> confirm;

    return ( confirm == "confirm" || confirm == "CONFIRM" );
}

std::string HashPassword(std::string pass)
{
  std::string hash;
  std::string cpy = pass;
  std::string remainder;

  while (cpy.size() >= 3)
  {
    remainder = cpy.substr(cpy.size() - 4, cpy.size());
    cpy = cpy.substr(0, cpy.size() - 4);
    hash += remainder[1];
  }

  hash += cpy;
  std::cout << "hash: " << hash << '\n';
  Sleep(1000);
  return hash;
}

bool IsEmptyFile(std::ifstream& file)
{
    return file.peek() == std::ifstream::traits_type::eof();
}

bool IsMarked(int opt)
{
  std::string path = "./data/" + std::to_string(GetCurrId()) + "_mark.txt";
  std::fstream file;
  std::string lane;
  file.open(path);
  int c = 0;
  getline(file, lane);
  file << lane;
  file.close();

  std::vector<int> marked = GetMarkedLaneIdxs(lane);

  int l = 0, r = marked.size() - 1;
  while (l <= r)
  {
    int m = (l + r) / 2;
    if (marked[m] == opt)
      return true;
    else if (marked[m] > opt)
      r = m - 1;
    else
      l = m + 1;
  }

  return false;
}

bool LoginUser(std::string* UsernameAndPassword)
{
  bool isApproved = false;
  std::string path = "./acc/", tUser, tPass;
  int j = 0;
  std::ifstream file;
  while (j < GetNextUserId())
  {
    file.open(path + std::to_string(j) + ".txt");
    getline(file, tUser);
    getline(file, tPass);

    // if (tUser == UsernameAndPassword[0] && HashPassword(tPass) == UsernameAndPassword[1])
    if (tUser == UsernameAndPassword[0] && tPass == UsernameAndPassword[1])
    {
      isApproved = true;
      break;
    }
    file.close();
    j++;
  }
  if (isApproved)
    SaveLocally(UsernameAndPassword[0], j);

  return isApproved;
}

void Mark()
{
  int mark = selectOption;
  if (not (IsMarked(mark)))
  {
    std::string path = "./data/" + std::to_string(GetCurrId()) + "_mark.txt";
    std::ifstream fileOut;
    fileOut.open(path);
    std::string lane;
    getline(fileOut, lane);

    lane += (' ' + std::to_string(mark));
    std::vector<int> marked = GetMarkedLaneIdxs(lane);
    lane = "";
    for (int m: marked)
      lane += (' ' + std::to_string(m));
    std::cout << lane << '\n';

    std::ofstream fileIn;
    fileIn.open(path, std::ios::trunc);
    fileIn << lane;
    fileIn.close();
  }
}

void NewNote()
{
  int lastElementIdx = GetNumberOfNotes();
  if (lastElementIdx + 1 > 99)
  {
    std::cout << char(7);
    return;
  }
  int nearestIdxToLastPage = (lastElementIdx / 10) * 10;
  std::string note;
  idx = nearestIdxToLastPage + 1;

  ShowCurrentNotes(GetData());

  std::cout << "\n  |" << ( (lastElementIdx + 1 < 10) ? " " : "" ) << lastElementIdx + 1 << "| ";
  HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(console, 7);
  getline(std::cin, note);
  if (note.size() == 0)
  {
    std::cout << char(7);
    NewNote();
  }
  std::string path = "./data/" + std::to_string(GetCurrId()) + ".txt";
  std::fstream file;
  file.open(path, std::ios::app | std::ios::ate);
  file << '\n' << note;
  SetConsoleTextAttribute(console, 15);
  file.close();
}

void OpenHelp()
{
  system("clear");
  std::cout << "         Help Menu       \n"
            << "  -----------------------\n"
            << "  |Key|     Action      |\n"
            << "  -----------------------\n"
            << "  | D | Mark as done    |\n"
            << "  | E | Edit            |\n"
            << "  | H | Open help       |\n"
            << "  | M | Mark            |\n"
            << "  | N | New             |\n"
            << "  | R | Remove          |\n"
            << "  | U | Unmark          |\n"
            << "  | V | Full view       |\n"
            << "  | Q | Quit            |\n"
            << "  -----------------------\n"
            << "\n"
            << " Press any key to return...\n";

  int keyCode = getch();
  return;
}

void RegisterUser(std::string UsernameAndPass[])
{
  std::string path = std::to_string(GetNextUserId()) + ".txt";
  std::ofstream file;
  file.open("./data/" + path);
  file.close();
  file.open("./acc/" + path);
  file << UsernameAndPass[0] << '\n';
  file << UsernameAndPass[1] << '\n';
  // file << HashPassword(UsernameAndPass[1]) << '\n';
  // std::cout << HashPassword(UsernameAndPass[1]) << '\n';
  file.close();
}

void RemoveNote()
{
  Sleep(150);
  std::string path = "./data/" + std::to_string(GetCurrId()) + ".txt";
  std::fstream fileOut;
  std::string lane;
  std::vector<std::string> backup;
  fileOut.open(path);
  int r = 0;
  int searched = std::max(toRemove, isDone);
  system("clear");
  // std::cout << "searched: " << searched << '\n';
  // Sleep(1000);
  while (!fileOut.eof() && r < 100)
  {
    r++;
    getline(fileOut, lane);
    if (r != searched)
      backup.push_back(lane);
  }

  fileOut.close();
  std::ofstream fileIn;
  fileIn.open(path, std::ios::trunc);

  for (int i = 0; i < backup.size() - 1; i++)
    fileIn << backup.at(i) << '\n';

  fileIn << backup.back();
  fileIn.close();

  toRemove = 0;
  isDone = 0;
  ShowCurrentNotes(GetData());
}

void ReqIsDone()
{
  isDone = selectOption;
}

void ReqRemoveNote()
{
  toRemove = selectOption;
}

void SaveLocally(std::string user, int id)
{
    std::ofstream file;
    std::string path = "./process/curr.txt";
    file.open(path, std::ios::trunc);
    file << user << '\n';
    file << id << '\n';

    file.close();
}

void ShowCurrentNotes(std::vector<std::string> data)
{
  system("clear");
  HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(console, 15);
  const int SPACE_LIMIT_CHARS = 17;
  int count = idx;

  std::cout << TITLE_PART;
  if (data.size() == 0 || data[0] == "")
  {
    std::cout << "     There are no notes     \n";
    std::cout << "    Press H to open help\n";
    return;
  }

  for (std::string lane: data)
  {
    std::cout << "  |" << ( (count < 10) ? " " : "" ) << count << '|';

    if (count == isDone)
    {
      SetConsoleTextAttribute(console, 2); // Green
    } else if (count == toRemove)
    {
      SetConsoleTextAttribute(console, 12); // Red
    } else if (selectOption == count)
      SetConsoleTextAttribute(console, 3); // Old style Blue-Green
    else if (IsMarked(count))
      SetConsoleTextAttribute(console, 14); // Yellow
    else
      SetConsoleTextAttribute(console, 15); // White

    std::cout << ' ';
    if (lane.size() > SPACE_LIMIT_CHARS)
      std::cout << lane.substr(0, 14) << "...";
    else if (lane.size() < SPACE_LIMIT_CHARS)
    {
        int diff = SPACE_LIMIT_CHARS - lane.size();
        for (int j = 0; j < diff; j++)
          lane.push_back(' ');
      std::cout << lane;
    }


    SetConsoleTextAttribute(console, 15);
    std::cout << " |\n";
    count++;
  }
  std::cout << "  ------------------------\n";
  SetConsoleTextAttribute(console, (selectOption == LEFT_ARROW_IDX) ? 9 : 15 );
  std::cout << "  <<";
  std::cout << "                    ";
  SetConsoleTextAttribute(console, (selectOption == RIGHT_ARROW_IDX) ? 9 : 15 );
  std::cout << ">>\n";

  SetConsoleTextAttribute(console, 15);
  if (GetNumberOfNotes() - idx <= 10)
  {
    std::cout << "\n    Press H to open help\n";
  }

  if (toRemove || isDone)
    RemoveNote();

  return;
}

int TrackKeyboardPress()
{
  int keyCode = -1;
  keyCode = getch();

  return keyCode;
}

int DrawHomePage()
{
  int op = 0, key = -1;
  HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
  selectOption = 0;
  while (1)
  {
    const std::string TWENTYEIGHT_DIEZ = std::string(28, '#');
    const std::string SEVEN_DIEZ = std::string(7, '#');
    const std::string FOURTEEN_SPACES = std::string(14, ' ');
    const std::string TEN_DASHES = std::string(10, '-');

    SetConsoleTextAttribute(console, 15);

    for (int i = 0; i < 3; i++)
      std::cout << TWENTYEIGHT_DIEZ << '\n';

    std::cout << SEVEN_DIEZ << FOURTEEN_SPACES << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " |";

    SetConsoleTextAttribute(console, (selectOption == 0) ? 3 : 15);
    std::cout << " Register ";
    SetConsoleTextAttribute(console, 15);
    std::cout << "| " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << FOURTEEN_SPACES << SEVEN_DIEZ << '\n';

    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " |";
    SetConsoleTextAttribute(console, (selectOption == 1) ? 3 : 15);
    std::cout << "  Login   ";
    SetConsoleTextAttribute(console, 15);
    std::cout << "| " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';

    std::cout << SEVEN_DIEZ << FOURTEEN_SPACES << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " |";
    SetConsoleTextAttribute(console, (selectOption == 2) ? 3 : 15);
    std::cout << "   Quit   ";
    SetConsoleTextAttribute(console, 15);
    std::cout << "| " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << " +" << TEN_DASHES << "+ " << SEVEN_DIEZ << '\n';
    std::cout << SEVEN_DIEZ << FOURTEEN_SPACES << SEVEN_DIEZ << '\n';

    for (int i = 0; i < 3; i++)
      std::cout << TWENTYEIGHT_DIEZ << '\n';

    SetConsoleTextAttribute(console, 15);
    key = getch();
    if (key == 224)
      ExecuteArrowOper(0, 2, true);
    else if (key == ENTER)
      break;
  }

  op = selectOption;

  return op;
}

bool RequireAuthentication()
{
  bool isApproved = false;

  int selectBackup = selectOption;
  int op = DrawHomePage();
  system("clear");
  selectOption = selectBackup;

  if (op == 0 || op == 1)
  {
    std::string* userAndPass = GetInput();
    if (op == 0)
      RegisterUser(userAndPass);
    else
      isApproved = LoginUser(userAndPass);
  } else if (op == 2) {
    Quit();
  }
  system("clear");

  // std::cout << std::to_string(isApproved) << '\n';
  // Sleep(1000);

  return isApproved;
}

void Unmark()
{
  std::ifstream fileIn;
  std::string path = "./data/" + std::to_string(GetCurrId()) + "_mark.txt";
  fileIn.open(path);
  std::string lane;
  getline(fileIn, lane);
  fileIn.close();

  std::vector<int> marked = GetMarkedLaneIdxs(lane);
  marked.erase(std::remove(marked.begin(), marked.end(), selectOption), marked.end());
  lane = "";
  for (int m: marked)
    lane += (' ' + std::to_string(m));

  std::ofstream fileOut;
  fileOut.open(path, std::ios::trunc);
  fileOut << lane;
  fileOut.close();
}

void ViewNote()
{
  system("clear");
  std::string path = "./data/" + std::to_string(GetCurrId()) + ".txt";
  std::ifstream file;
  file.open(path);
  int i = 1;
  std::string lane;
  while (!file.eof())
  {
    getline(file, lane);
    if (i == selectOption)
      break;
    ++i;
  }
  file.close();
  std::cout << "      Your full note:\n\n";
  while (lane.size() > 25)
  {
    std::string curr = lane.substr(0, 26);
    if (curr[0] == SPACE)
      curr = curr.substr(1, 26);
    std::cout << ' ' << curr << " \n";
    lane = lane.substr(26);
  }
  std::cout << " " << lane;
  std::cout << "\n\n Press any key to return...\n";
  int key = getch();
}

void Quit()
{
  system("clear");
  std::cout << "Bye! :)\n";
  exit(0);
}
